from turtle import Turtle
from pip._vendor.typing_extensions import Self
STARTING_POSITION = [(0,0),(-20,0),(-40,0)]
MOVE_DISTANCE = 20


class Snake:
    def __init__(self):
        self.segmants = []
        self.greate_snake()
        self.head = self.segmants[0]
    
    def reset(self):
        for seg in self.segmants:
            seg.goto(1000,1000)
        self.segmants.clear()
        self.greate_snake()
        self.head = self.segmants[0]
    
    def extend(self):
        #add a new segment to the snake
        self.add_segment(self.segmants[-1].position())
    
    def add_segment(self,position):
        new_segment = Turtle(shape="square")
        new_segment.color("white")
        new_segment.penup()
        new_segment.goto(position)
        self.segmants.append(new_segment)

    def greate_snake(self):
        for position in STARTING_POSITION:
            self.add_segment(position)
          
    def move(self):
        for seg_num in range(len(self.segmants) - 1,0,-1):
            new_x = self.segmants[seg_num -1].xcor()
            new_y = self.segmants[seg_num - 1].ycor()
            self.segmants[seg_num].goto(new_x,new_y)
        self.segmants[0].forward(MOVE_DISTANCE)
    def up(self):
        if self.segmants[0].heading() == 0.0:
            self.segmants[0].left(90)
        elif self.segmants[0].heading() == 180.0:
            self.segmants[0].left(-90)
    def down(self):
        if self.segmants[0].heading() == 0.0:
            self.segmants[0].right(90)
        elif self.segmants[0].heading() == 180.0:
            self.segmants[0].left(90)
    def left(self):
        if self.segmants[0].heading() == 90.0:
            self.segmants[0].left(90)
        elif self.segmants[0].heading() == 270.0:
            self.segmants[0].right(90)
    def right(self):
        if self.segmants[0].heading() == 90.0:
            self.segmants[0].right(90)
        elif self.segmants[0].heading() == 270.0:
            self.segmants[0].left(90)
        
