from turtle import Turtle
from msilib.schema import File
ALIGNMENT = "center"
FONT = ("Ariel",24,"normal")



class Score(Turtle):
    
    def __init__(self):
        self.score = 0
        super().__init__()
        with open("data.txt") as file:
            self.high_score = int(file.read())
            print(self.high_score)
        self.hideturtle()
        self.penup()
        self.color("white")
        self.goto(x = 0, y = 260)
        self.update_score()
    
    def update_score(self):
        self.clear()
        self.write(f"Score = {self.score} High Score: {self.high_score}", False, ALIGNMENT, FONT)
        
    def reset(self):
        if self.score > self.high_score:
            self.high_score = self.score
            with open("data.txt",mode="w") as file:
                file.write(f"{self.high_score}")
        self.score = 0
        self.update_score()
        
        
    '''def game_over(self):
        self.goto(x = 0, y = 0)
        self.write("GAME OVER",align=ALIGNMENT,font=FONT)
    '''    
        
    def increaseScore(self):
        self.score +=1
        self.update_score()
        